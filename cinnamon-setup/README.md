# cinnamon-setup

This repository contains my personal setup for Cinnamon desktop environment.

## Important changes
 - `Ctrl+Alt+T` doesn't launch the default terminal, it executes `terminator --new-tab`

## Files

`all.settings` - dump of the Cinnamon settings

```
$ dconf dump /org/cinnamon > all.settings
```

`cinnamon-menu.json` - export of the cinnamon menu (`menu@cinnamon.org`)

`/oreo_black_bordered_cursors` - the custom cursor I use

`cinnamon-setup.sh` - script that automatically installs most configurations
