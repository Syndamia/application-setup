#!/bin/bash

if [ "$EUID" = 0 ]
	then echo "Please do not run the script as root" && echo -en "\007" && spd-say "Please do not run the script as root"
	exit
fi

dconf load / < ./all.settings
~/.cinnamon/configs/menu@cinnamon.org/0.json < ./cinnamon-menu.json
~/.cinnamon/configs/calendar@cinnamon.org/12.json < ./calendar.json

sudo mv -r oreo_black_bordered_cursors /usr/share/icons
# Make the custom cursors show up on programs that they don't work on
sudo sed -i "s/Inherits=Mint-Y-Dark,Adwaita,gnome,hicolor/Inherits=oreo_black_bordered_cursors,Mint-Y-Dark,Adwaita,gnome,hicolor/" /usr/share/icons/Mint-Y-Dark-Blue/index.theme
# Transparent panel in Mint-Y-Dark-Blue theme
sudo sed -i "s/#panel {/#panel {\n  background-color: rgba(0, 0, 0, 0.07);/" /usr/share/themes/Mint-Y-Dark-Blue/cinnamon/cinnamon.css

echo "Done"
