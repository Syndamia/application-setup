#!/bin/sh

# ================================================================================
# This is a setup script for all utilities I need for the talk (presentation)
# https://syndamia.com/talks/linuxgraphy-by-strabo/
#
# Note that all of this can be done with a proper bundle and rebuild, but I'm lazy
# ================================================================================

#
# prerequisites
#
mkdir /tmp/linuxography-setup/
cd /tmp/linuxography-setup/

#
# package installation (all done in /usr/local)
#
mkdir /usr/local/
export PATH=$PATH:/usr/local/sbin:/usr/local/bin

# bash
wget -q http://s.minos.io/archive/bifrost/x86_64/bash-4.4-1.tar.xz
tar xf bash-4.4-1.tar.xz

# coreutils
wget -q http://s.minos.io/archive/bifrost/x86_64/coreutils-7.6-5.tar.xz
tar xf coreutils-7.6-5.tar.xz

# shadow
wget -q http://s.minos.io/archive/bifrost/x86_64/shadow-4.1.4.2-1.tar.xz
tar xf shadow-4.1.4.2-1.tar.xz

mv ./bin/ /usr/local/bin/
mv ./usr/sbin/ /usr/local/sbin/  # shadow
mv ./usr/bin/* /usr/local/bin/   # shadow

#
# file setup
#
tee /etc/passwd << EOF >/dev/null
root:x:0:0:root:/root:/bin/sh
bin:x:1:1:bin:/bin:/bin/false
daemon:x:2:2:daemon:/sbin:/bin/false
adm:x:3:4:adm:/var/adm:/bin/false
lp:x:4:7:lp:/var/spool/lpd:/bin/false
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
nobody:x:65534:65534::/dev/null:/bin/false
EOF

tee /etc/bashrc << EOF >/dev/null
export PS1='[\u] j:\j e:\$? \w \\$ '  # Prompt to the left of cursor

bind '"\e[A":history-search-backward' # UpArrow autocompletes partial history
bind '"\e[B":history-search-forward'  # DownArrow autocompletes partial history
bind '"\e[1;5C":forward-word'         # Ctrl+RightArrow jumps to next word
bind '"\e[1;5D":backward-word'        # Ctrl+LeftArrow jumps to predvious word
bind '"\C-h":backward-kill-word'      # Ctrl+BackSpace deletes an entire word

clear
cat /etc/motd
EOF

tee /etc/profile << EOF >/dev/null
export PATH=$PATH
source /etc/bashrc
EOF

#
# finalize
#
echo "Done!"
cd /
bash -l
