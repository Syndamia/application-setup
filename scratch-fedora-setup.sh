#!/bin/bash

# This is a script for installing most programs and setting (some of) them up,
# starting with, what I call, a scratch installation - making a "Minimal installation" via the Fedora 34 netinstaller, without any additional packages

# TODO:
# lock screen - i3lock-color
# Bluetooth - permissions fix
# Other application - xcape, lwa-hot-corners, xkbmon
# Gaming - runelite, minecraft

# ++=============++
# || Preparation ||
# ++=============++

scriptdir=$(pwd)
cd /tmp
sudo dnf upgrade -y

# +---------------+
# | Main packages |
# +---------------+

must_have="*ntfs* git curl dnf-utils util-linux-user"
laptop="NetworkManager-wifi tlp powertop brightnessctl xinput"
utilities="dbus-tools screenfetch neofetch xdotool setxkbmap xss-lock xev yad ImageMagick htop cloc"

x11="xorg-x11-drv-libinput xorg-x11-server-utils xorg-x11-server-common xinit xrdb xorg-x11-drv-*"
desktop_environment="openbox wmctrl picom feh tint2 volumeicon jgmenu terminator arandr"
cinnamon_packages="nemo nemo-fileroller nemo-terminal nemo-preview cinnamon-themes"

gnome_apps="gnome-disks gnome-system-monitor eog gnome-keyring seahorse"
apps="barrier cheese copyq audacious gimp samba adb flameshot piper gstreamer1-plugin-openh264 moc mpv libreoffice redshift-gtk"
qemukvm="qemu-kvm bridge-utils virt-manager ebtables iptables dnsmasq"
programming="haskell-platform godot vim-X11 nodejs"
gaming="steam lutris"

sudo dnf install -y $must_have $laptop $utilities $x11, $desktop_environment $cinnamon_packages $gnome_apps $apps $qemukvm $programming $gaming

# +--------------+
# | Login screen |
# +--------------+

sudo dnf install -y xdm sessreg
sudo systemctl enable xdm
wget https://filite.syndamia.com/t/xres && sudo cat xres > /etc/X11/xdm/Xresources && rm xres

echo 'exec openbox-session' > .xsession
chmod +x .xsession

sudo systemctl set-default graphical.target

# +-----------+
# | Bluetooth |
# +-----------+

sudo dnf install -y gnome-bluetooth blueberry rfkill

# +----------+
# | Gestures |
# +----------+

sudo gpasswd -a $USER input && sudo dnf install -y xdotool wmctrl
cd /tmp && git clone https://github.com/bulletmark/libinput-gestures.git && cd libinput-gestures/
sudo make install && libinput-gestures-setup autostart && libinput-gestures-setup start

# +--------------------+
# | Language and fonts |
# +--------------------+

# Bulgarian language
sudo dnf install -y langpacks-bg langpacks-core-bg langpacks-core-font-bg libreoffice-langpack-bg

# Fonts
sudo dnf install -y google-noto-sans-fonts gdouros-symbola-fonts
sudo dnf install -y google-noto-emoji-color-fonts && curl -fsSL https://hamptonmoore.com/tools/fedora32InstallColoredEmojis.sh | sh

# +-------+
# | Shell |
# +-------+

# Oh-my-zsh
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
curl -L https://raw.githubusercontent.com/sbugzu/gruvbox-zsh/master/gruvbox.zsh-theme > ~/.oh-my-zsh/custom/themes/gruvbox.zsh-theme

# Zsh (inspired by https://christitus.com/zsh/)
sudo dnf install -y zsh zsh-syntax-highlighting autojump zsh-autosuggestions && touch "$HOME/.cache/zshhistory"
if [ -f "/bin/zsh" ]; then chsh -s /bin/zsh $USER; else echo "ZSH wasn't installed correctly"; fi

# MesloLGS font
sudo mkdir meslolgs && cd meslolgs
sudo wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf
cd ../ && sudo mv meslolgs /usr/share/fonts

# +----------------------------+
# | Communication applications |
# +----------------------------+

# Discord
wget -O discord.tar.gz "https://discord.com/api/download?platform=linux&format=tar.gz" && tar -xzf discord.tar.gz && sudo mv Discord /opt

# Tutanota desktop
# wget "https://mail.tutanota.com/desktop/tutanota-desktop-linux.AppImage" && sudo mv tutanota-desktop-linux.AppImage /opt && sudo chmod +x /opt/tutanota-desktop-linux.AppImage && wget -O tutanota-desktop.png "https://raw.githubusercontent.com/tutao/tutanota/master/resources/desktop-icons/icon/512.png" && sudo mv tutanota-desktop.png /usr/share/icons

# ElectronMail
sudo dnf install -y "https://github.com/vladimiry/ElectronMail/releases/download/v4.12.3/electron-mail-4.12.3-linux-x86_64.rpm"

# +----------+
# | Browsers |
# +----------+

# Min
sudo dnf install -y "https://github.com/minbrowser/min/releases/download/v1.17.3/min-1.17.3-1.x86_64.rpm"

# Brave
sudo dnf config-manager --add-repo https://brave-browser-rpm-release.s3.brave.com/x86_64/ && sudo rpm --import https://brave-browser-rpm-release.s3.brave.com/brave-core.asc && sudo dnf install -y brave-browser

# Rambox
wget -O Rambox.AppImage "https://github.com/ramboxapp/community-edition/releases/download/0.7.7/Rambox-0.7.7-linux-x86_64.AppImage" && sudo mv Rambox.AppImage /opt && sudo chmod +x /opt/Rambox.AppImage

# +------------------------------------------+
# | Sharing and synchronization applications |
# +------------------------------------------+

# Teamviewer
sudo dnf install -y "https://download.teamviewer.com/download/linux/teamviewer.x86_64.rpm"

# scrcpy
sudo dnf copr enable zeno/scrcpy -y && sudo dnf -y install scrcpy

# Nextcloud desktop
wget -O nextcloud-desktop.AppImage "https://github.com/nextcloud/desktop/releases/download/v3.0.3/Nextcloud-3.0.3-x86_64.AppImage" && sudo mv nextcloud-desktop.AppImage /opt && sudo chmod +x /opt/nextcloud-desktop.AppImage

# Joplin
wget -O - https://raw.githubusercontent.com/laurent22/joplin/master/Joplin_install_and_update.sh | bash

# +--------------------+
# | Other applications |
# +--------------------+

# youtube-dl
sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl && sudo chmod a+rx /usr/local/bin/youtube-dl

# OBS studio (if you are running it separately, run it with bash)
sudo dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm && sudo dnf install -y obs-studio

# winehq (because wine from fedora packages can suck my !#@*&)
sudo dnf -y install dnf-plugins-core && sudo dnf config-manager --add-repo https://dl.winehq.org/wine-builds/fedora/34/winehq.repo && sudo dnf -y install winehq-stable

# winetricks
wget https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks && chmod +x winetricks && sudo mv winetricks /usr/local/bin/

# barrier
sudo dnf install -y barrier && sudo firewall-cmd --add-port=24800/tcp && sudo firewall-cmd --runtime-to-permanent

# +-----------------------+
# | Programming utilities |
# +-----------------------+

# Vim-plug
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# Gruvbox theme fix
mkdir ~/.vim/colors/; ln -s /home/kamen/.vim/plugged/gruvbox/colors/gruvbox.vim ~/.vim/colors/

# lazygit
sudo dnf copr enable atim/lazygit -y && sudo dnf install -y lazygit

# MySQL (refer to https://stackoverflow.com/a/52216111/12036073 if you want to set a weak password, ONLY DO IT FOR TESTING PURPOSES)
sudo dnf install https://repo.mysql.com//mysql80-community-release-fc31-1.noarch.rpm && sudo dnf install -y mysql-community-server && sudo systemctl start mysqld && sudo systemctl enable mysqld && echo "Your root temporary password: " && sudo grep 'temporary password' /var/log/mysqld.log && sudo mysql_secure_installation

# ++============++
# || Finalizing ||
# ++============++

sudo dnf autoremove -y

echo
echo "Done! Please reboot!"
echo
