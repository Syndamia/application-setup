#
# These are the general packages I use on my desktop setup, exluding most specific application I use:
# - window manager + compositor
# - panel
# - notification daemon
# - login manager
# - two terminal emulators
# - GUI configuration utilities
# - pipewire + audio plugins
# - fonts
# - gtk themes
#
# All other desktop applications, like browser and file manager, are listed in @my-gui
#

#
# desktop functionality
#
x11-wm/openbox # window manager
x11-misc/picom # compositor
x11-misc/tint2 # panel
media-gfx/feh  # sets desktop background
x11-misc/dunst # notification daemon

# important
sys-apps/dbus  # system for inter-application communication
x11-libs/cairo # vector graphics library

# terminal emulators
x11-terms/xterm

# pannel applets / systray apps
dev-libs/libappindicator # library for system tray icons
gnome-extra/nm-applet    # network management
media-sound/pnmixer      # volume mixer

# gestures
dev-libs/libinput               # library to handle input devices
x11-drivers/xf86-input-libinput # X.org input drivers
x11-misc/libinput-gestures      # touchpad gesture control

#
# login, locking, authentication
#
x11-misc/lightdm             # display/login manager
x11-misc/lightdm-gtk-greeter
x11-misc/xautolock           # configure how to lock your screen
sys-auth/elogind             # systemd's extracted logind, control poweroff, reboot, ...
lxqt-base/lxqt-policykit     # polkit authentication engine, handle applications asking for root password on non-root user

#
# settings edit utilities
#
x11-misc/devilspie2            # do scripted actions on window creation
x11-misc/xcape                 # make modifier keys send custom key events when pressed on their own
x11-apps/setxkbmap # control keyboard layout (of an X server)

# GUI editors
lxde-base/lxappearance         # GTK+ theme switcher
x11-misc/obconf                # openbox configuration utility
lxde-base/lxappearance-obconf
net-wireless/blueberry         # bluetooth configuration tool
app-misc/piper                 # gaming mice configuration
x11-misc/arandr                # monitor configuration

#
# audio
#
media-video/pipewire        # partly a sound server, refer to https://wiki.gentoo.org/wiki/PipeWire
media-sound/pavucontrol     # volume control mixer for pulseaudio
media-libs/gst-plugins-good # plugin for gstreamer, library for handling media, used by a lot of music and video players
media-libs/gst-plugins-ugly
media-plugins/gst-plugins-assrender
media-plugins/gst-plugins-dash
media-plugins/gst-plugins-faad
media-plugins/gst-plugins-gtk
media-plugins/gst-plugins-hls
media-plugins/gst-plugins-jpeg
media-plugins/gst-plugins-ladspa
media-plugins/gst-plugins-libde265
media-plugins/gst-plugins-libnice
media-plugins/gst-plugins-lv2
media-plugins/gst-plugins-modplug
media-plugins/gst-plugins-mpg123
media-plugins/gst-plugins-pulse
media-plugins/gst-plugins-v4l2
media-plugins/gst-plugins-vpx

#
# fonts
#
media-fonts/fonts-meta
x11-base/xorg-fonts
virtual/ttf-fonts
media-fonts/arphicfonts
#media-fonts/ipamonafont # cannot be fetched?
media-fonts/noto
media-fonts/noto-emoji
media-fonts/ja-ipafonts
media-fonts/takao-fonts
media-fonts/wqy-microhei
media-fonts/wqy-zenhei
media-fonts/jetbrains-mono

#
# themes
#
x11-themes/gtk-engines          # gtk+2.0 standard themes and theme engines
x11-themes/gtk-engines-candido  # gtk+2.0 theme engine
x11-themes/gtk-engines-rezlooks
x11-themes/gtk-engines-murrine
