#!/bin/bash
# This script mostly removes (and sometimes moves) folders, to prepare for a new OS install (primarily for NixOS with my config).

echo "Please review and run these commands manually!"; exit 1

rm -r \
	/bin /sbin /lib /lib64 \
	/usr/bin /usr/sbin /usr/lib /usr/lib64 /usr/libexec \
	/usr/share/applications /usr/share/fonts \
	/var/db /var/cache \
	/etc/elogind /etc/polkit-1

cp -r /usr/share/themes/Mint* /home/kamen/.local/share/themes/
cp -r /usr/share/themes/Blocks /home/kamen/.local/share/themes/
cp -r /usr/share/icons/Mint* /home/kamen/.local/share/icons/
cp -r /usr/share/icons/oreo* /home/kamen/.local/share/icons/
